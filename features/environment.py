import os
from dotenv import load_dotenv
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from behave import fixture, use_fixture


# Load environment variables from .env file
load_dotenv()


@fixture
def browser_chrome(context):
    """Start Chrome browser"""
    # Configure Chrome options
    chrome_options = Options()
    # chrome_options.add_argument("--headless")  
    chrome_options.add_argument("--disable-gpu")
    chrome_options.add_argument("--no-sandbox")
    chrome_options.add_experimental_option("detach", True)

    # Initialize Chrome WebDriver
    context.browser = webdriver.Chrome(options=chrome_options)

    # Set up implicitly wait time
    context.browser.implicitly_wait(10)  # 10 seconds wait time

    yield context.browser

    # Teardown: Quit the browser after each scenario
    # context.browser.quit()


def before_all(context):
    """Run before all scenarios"""
    use_fixture(browser_chrome, context)
    context.base_url = os.environ.get('BASE_URL', 'http://localhost:8000')
    context.user = {
        'username': None,
        'firstname': None,
        'lastname': None,
        'email': None,
        'password': None,
    }
