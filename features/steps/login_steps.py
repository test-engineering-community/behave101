from behave import given, when, then


@when('I enter my username and password')
def step_impl(context):
    context.execute_steps(
        '''
        When I enter "lexbryan" in the "username" field
        And I enter "13xbry@N" in the "password" field
        '''
    )


@when('I enter an invalid username or password')
def step_impl(context):
    context.execute_steps(
        '''
        when I enter "invalid" in the "username" field
        and I enter "13xby@N" in the "password" field
        '''
    )


@when('I enter a registered username and password')
def step_impl(context):
    context.execute_steps(
        f'''
        When I enter "{context.user['username']}" in the "username" field
        And I enter "{context.user['password']}" in the "password" field
        '''
    )


@given('I am logged out')
def step_impl(context):
    context.execute_steps('''I am on the "/logout" page''')
