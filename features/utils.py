import random
import string
import re


def generate_random_str(valid_special_chars="") -> str:
    """
        Generates a random str that satisfies the following criteria:
        - Contains at least 8 characters.
        - Contains at least one lowercase letter, one uppercase letter,
          one digit, and one special character (punctuation).
        - Is not entirely numeric.

        Returns:
            str: A random str.
        """
    # Define the pool of characters to choose from for the password
    characters = string.ascii_letters + string.digits + valid_special_chars

    while True:
        # Generate a random password with at least 8 characters
        random_str = ''.join(random.choice(characters) for _ in range(8))

        # Check if the password meets the criteria
        if (
            any(c.islower() for c in random_str) and
            any(c.isupper() for c in random_str) and
            any(c.isdigit() for c in random_str) and
            not random_str.isnumeric()
        ):
            # Remove invalid characters
            random_str = ''.join(c for c in random_str if c in characters)
            return random_str


def generate_random_email():
    return f'{generate_random_str()}@gmail.com'

